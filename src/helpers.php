<?php

if (!function_exists('get_real_class')) {
    /**
     * Obtains an object class name without namespaces
     */
    function get_real_class($obj)
    {
        $classname = is_string($obj) ? $obj : get_class($obj);

        if (preg_match('@\\\\([\w]+)$@', $classname, $matches)) {
            $classname = $matches[1];
        }

        return $classname;
    }
}

if (!function_exists('mask_text_with_stars')) {
    function mask_text_with_stars($your_string)
    {
        return preg_replace("/(^.|.$)(*SKIP)(*F)|(.)/", "*", $your_string);
    }
}

if (!function_exists('mask')) {

// Usage:
//  $string = '1234 5678 9123 45678';
//  echo mask($string,null,strlen($string)-4); // *************5678
    function mask($str, $start = 0, $length = null)
    {
        $mask = preg_replace("/\S/", "*", $str);
        if (is_null($length)) {
            $mask = substr($mask, $start);
            $str = substr_replace($str, $mask, $start);
        } else {
            $mask = substr($mask, $start, $length);
            $str = substr_replace($str, $mask, $start, $length);
        }
        return $str;
    }
}
if (!function_exists('mask_phone_number_with_stars')) {

    function mask_phone_number_with_stars($number)
    {
        $middle_string = "";
        $length = strlen($number);

        if ($length < 3) {

            return $length == 1 ? "*" : "*" . substr($number, -1);

        } else {
            $part_size = floor($length / 3);
            $middle_part_size = $length - ($part_size * 2);
            for ($i = 0; $i < $middle_part_size; $i++) {
                $middle_string .= "*";
            }

            return substr($number, 0, $part_size) . $middle_string . substr($number, -$part_size);
        }
    }
}


if (!function_exists('mask_email_with_stars')) {
    function mask_email_with_stars($email)
    {
//        $email = preg_replace('/^.\K|.(?=.*@)|@.\K|\..*(*SKIP)(*F)|.(?=.*\.)/', '*', $email);
        $email = preg_replace('/(?:^|@).\K|\.[^@]*$(*SKIP)(*F)|.(?=.*?\.)/', '*', $email);
        return $email;
    }
}

if (!function_exists('mask_email_with_stars_2')) {
    function mask_email_with_stars_2($email)
    {
        $stars = 4; // Min Stars to use
        $at = strpos($email, '@');
        if ($at - 2 > $stars) $stars = $at - 2;
        return substr($email, 0, 1) . str_repeat('*', $stars) . substr($email, $at - 1);
    }
}
