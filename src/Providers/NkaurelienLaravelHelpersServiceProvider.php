<?php

namespace  Nkaurelien\Momopay\Providers;

use Illuminate\Support\ServiceProvider;
use Nkaurelien\Helpers\Console\Commands\DatabaseNotificationClear;
use Nkaurelien\Helpers\Console\Commands\PassportDeleteRevokedTokens;
use Nkaurelien\Helpers\Console\Commands\RoleAddUser;
use Nkaurelien\Helpers\Console\Commands\UserFindCommand;
use Nkaurelien\Helpers\Console\Commands\UserPasswordCommand;

class NkaurelienLaravelHelpersServiceProvider extends ServiceProvider
{


    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {

        $this->registerArtisanCommand();

    }

//    /**
//     * Register config.
//     *
//     * @return void
//     */
//    protected function registerConfig()
//    {
//        $this->mergeConfigFrom(
//            __DIR__ . '/../Config/config.php', 'momopay'
//        );
//        $this->publishes([
//            __DIR__ . '/../Config/config.php' => config_path('momopay.php'),
//        ], 'momopay-config');
//    }

    /**
     * Register commands.
     *
     * @return void
     */
    public function registerArtisanCommand()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                RoleAddUser::class,
                UserFindCommand::class,
                UserPasswordCommand::class,
                DatabaseNotificationClear::class,
                PassportDeleteRevokedTokens::class,
            ]);
        }
    }


    // /**
    //  * Register Database.
    //  *
    //  * @return void
    //  */
    // public function databases()
    // {
    //     $this->loadMigrationsFrom(__DIR__.'/../stuff/migrations');
    //     $this->publishes([
    //         __DIR__ . '/../stuff/migrations/' => database_path('migrations'),
    //     ], 'momopay-database');
    // }

}
