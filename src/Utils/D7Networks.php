<?php
/**
 * Created by PhpStorm.
 * User: nkaurelien
 * Date: 27/02/19
 * Time: 12:13
 */

namespace Nkaurelien\Helpers\Utils;


use Httpful\Mime;
use Illuminate\Support\Str;

class D7Networks
{
    /**
     * @return array|object|string
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public static function smsBalance()
    {
        $url = 'https://rest-api.d7networks.com/secure/balance';

        return \Httpful\Request::get($url)
            ->addHeaders(self::getHeaders())
            ->send()->body;
    }

    /**
     * @return array|object|string
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    private static function getHeaders()
    {
        return [
            'Authorization' => 'Basic YWpkbjk5NzA6TGRFNlpqc1Q=',
            'Content-Type' => 'application/x-www-form-urlencoded',
        ];
    }

    /**
     * @return array|object|string
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public static function sendSMS($to, $message)
    {

        $url = 'https://rest-api.d7networks.com/secure/send';
        $from = Str::ucfirst(Str::camel(config('app.name')));

        $body = JSON::encode([
            'from' => $from,
            'to' => '+' . ltrim("$to", '+'),
            'content' => $message,
//            'content' => utf8_encode($message),
        ]);
//        dd('ok', $from, $to);

        return \Httpful\Request::post($url, $body, Mime::JSON)
            ->withoutStrictSsl()
            ->addHeaders(self::getHeaders())
            ->send()->body;
    }

    /**
     * @return array|object|string
     * @throws \Httpful\Exception\ConnectionErrorException
     */
    public static function sendbatchSMS(array $to, $message)
    {

        $url = 'https://rest-api.d7networks.com/secure/sendbatch';

        $messages = collect($to)->map(function ($tel) use ($message) {
            return [
                'from' => Str::ucfirst(Str::camel(config('app.name'))),
                'to' => $tel,
                'content' => $message,
            ];
        });
        return \Httpful\Request::post($url)
            ->body(JSON::encode([
                'messages' => $messages,
            ]))
            ->addHeaders(self::getHeaders())
            ->send()->body;
    }
}
