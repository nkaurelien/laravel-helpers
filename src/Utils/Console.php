<?php


namespace Nkaurelien\Helpers\Utils;


use Symfony\Component\Console\Output\ConsoleOutput;

class Console
{

    public static function writeLn($mes)
    {
        $out = new ConsoleOutput();
        $out->writeln(is_array($mes) ? JSON::encode($mes) : $mes);
    }
}