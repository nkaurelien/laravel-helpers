<?php

namespace Nkaurelien\Helpers\Utils;


class Random
{

    /**
     * @param $length
     * @return string
     * @throws \Exception
     */
    public static function randomNumber($length)
    {
//        $characters = '0123456789';
        $characters = '2345678';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randstring;
    }

    /**
     * @param $length
     * @return string
     * @throws \Exception
     */
    public static function randomString($length)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < $length; $i++) {
            $randstring .= $characters[random_int(0, strlen($characters) - 1)];
        }
        return $randstring;
    }


}
