<?php


namespace Nkaurelien\Helpers\Utils;

use Illuminate\Support\Facades\File;


class Fichier
{

    /**
     * Cette fonction est utilisé pour charger automatiquement des fichiers  decouper et reorganiser dans les sous repertoires
     * 
     * 
     * File::load('api')
     */
    public static function requireAllFiles($prefix)
    { 
        foreach (File::allFiles(__DIR__ . '/' . $prefix ) as $file) {
            require $file->getPathname();
        }
    }
}