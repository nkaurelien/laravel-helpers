<?php

namespace Nkaurelien\Helpers\Utils;


class DataHelper {

    public static function pourcentage($nbre,$nbreTotal){
        return round(($nbre / (empty($nbreTotal) ? 1 : $nbreTotal)) * 100);
    }

}