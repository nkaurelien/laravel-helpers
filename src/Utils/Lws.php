<?php
/**
 * Created by PhpStorm.
 * User: nkaurelien
 * Date: 27/02/19
 * Time: 12:13
 */

namespace Nkaurelien\Helpers\Utils;



class Lws
{


    static function smscredit_lws($username,$password, $content_type = 'json')
    {
        if (app()->environment() === "production") {
            $sms=@file("https://sms.lws-hosting.com/api/checkcredits/$username/$password/$content_type");

            if ($sms[0] != 'Error') {
                echo "votre credit sms lws est de $sms[1] Euros";
            } else {
                echo 'Erreur:'.$sms[0].$sms[1];
            }
        }
    }

    static function sms_lws($username,$password,$message,$expediteur,$destinataire, $content_type = 'text')
    {

        if (app()->environment() === "production") {
            $message = urlencode($message);
            $sms=@file("https://sms.lws-hosting.com/api/sendsms/$username/$password/$content_type/$expediteur/$destinataire/$message");


//        print(collect($sms));
            if ($sms[0] != 'Error') {
                return true;
//                print 'votre sms est envoye';
//            $res = \json_decode($sms[0], true);
//            print $res['status'];
            } else {
                return false;
//            print 'Erreur:'.$sms[0].$sms[1];
            }
        }

    }


    static function sms_lws_alt($username,$password,$message,$expediteur,$destinataire, $content_type = 'text')
    {
        $message = urlencode($message);
        $url = "https://sms.lws-hosting.com/api/sendsms/$username/$password/$content_type/$expediteur/$destinataire/$message";


        $response = @file_get_contents($url);

        if ($response != 'Error') {
//            echo 'votre sms est envoye';
            return true;
        } else {
//            echo 'Erreur:'.$response;
            return false;
        }

    }

}