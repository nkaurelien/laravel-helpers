<?php
/**
 * Created by PhpStorm.
 * User: nkaurelien
 * Date: 27/02/19
 * Time: 10:34
 */

namespace Nkaurelien\Helpers\Utils;


use Illuminate\Support\Str;

class Phone
{


    /**
     * @param $phoneNumber
     * @param string $countryPhoneCode
     * @param bool $returnWithPhoneCountryCode
     * @return string
     */
    public static function purifyPhoneNumberForFirebase($phoneNumber, $countryPhoneCode = "+237", $returnWithPhoneCountryCode = true)
    {
        return '+' . ltrim(self::purifyPhoneNumber($phoneNumber, $countryPhoneCode, $returnWithPhoneCountryCode), '+');
    }

    public static function purifyPhoneNumber($phoneNumber, $countryPhoneCode = "237", $returnWithPhoneCountryCode = true)
    {
        $countryPhoneCode = ltrim($countryPhoneCode, '+');
        $countryPhoneCode = ltrim($countryPhoneCode, '00');
        $countryPhoneCode = str_replace(' ', '', $countryPhoneCode);
        $phoneNumber = str_replace(' ', '', $phoneNumber);

//        Todo only work with Laravel 7
//        $countryPhoneCode = Str::of($countryPhoneCode)->replaceMatches('/[^A-Za-z0-9]++/', '');
//        $phoneNumber = Str::of($phoneNumber)->replaceMatches('/[^A-Za-z0-9]++/', '');

        if (Str::startsWith($phoneNumber, $countryPhoneCode)
            || Str::startsWith($phoneNumber, '+' . $countryPhoneCode)
            || Str::startsWith($phoneNumber, '00' . $countryPhoneCode)
            || Str::startsWith($phoneNumber, '(' . $countryPhoneCode . ')')
            || Str::startsWith($phoneNumber, '+(' . $countryPhoneCode . ')')
            || Str::startsWith($phoneNumber, '00(' . $countryPhoneCode . ')')) {

            $phoneNumber = preg_replace('/^\+?/', '', $phoneNumber);
            $phoneNumber = preg_replace('/^(00)?/', '', $phoneNumber);
            $phoneNumber = preg_replace('/^' . $countryPhoneCode . '/', '', $phoneNumber);
            $phoneNumber = preg_replace('/^\(' . $countryPhoneCode . '\)/', '', $phoneNumber);

        }
        $countryPhoneCode = $returnWithPhoneCountryCode ? $countryPhoneCode : '';


        return "{$countryPhoneCode}{$phoneNumber}";


    }


    /**
     * Dynamically handle calls into the query instance.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \BadMethodCallException
     */
    public static function __callStatic($method, $parameters)
    {
        return call_user_func_array($method, $parameters);
    }

}
