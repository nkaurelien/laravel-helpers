<?php


namespace Nkaurelien\Helpers\Traits;


use Illuminate\Support\Facades\Log;

trait HasErrorLogging
{


    /**
     * Perform the given callback with exception handling.
     *
     * @param \Closure $callback
     * @return mixed
     *
     */
    protected function withErrorHandling($callback)
    {
        try {
            return $callback();
        } catch (\Throwable $e) {
            $this->logError($e);
            return null;
        }
    }

    /**
     * Perform exception logging.
     *
     * @param \Closure $callback
     * @return mixed
     *
     */
    protected function logError($error)
    {
        Log::error($error->getMessage(), [
            'FILE' => $error->getFile(),
            'LINE' => $error->getLine(),
            'CODE' => $error->getCode(),
            'CLASS' => get_real_class($error),
        ]);

        // TODO send to sentry or slack channel or other remote error monitoring system
//        if ($error instanceof Exception) {
//            @Bugsnag::notifyException($error);
//        }
    }


}
