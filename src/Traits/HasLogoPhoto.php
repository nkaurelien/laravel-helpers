<?php


namespace Nkaurelien\Helpers\Traits;


use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

trait HasLogoPhoto
{
    /**
     * Update the user's tradeRegister photo.
     *
     * @param UploadedFile $photo
     * @return void
     */
    public function updateLogo(UploadedFile $photo)
    {
        tap($this->logo, function ($previous) use ($photo) {
            $this->forceFill([
                'logo' => $photo->storePublicly(
                    'enterprises-photos', ['disk' => $this->logoDisk()]
                ),
            ])->save();

            if ($previous) {
                Storage::disk($this->logoDisk())->delete($previous);
            }
        });
    }

    /**
     * Update the user's tradeRegister photo.
     *
     * @param UploadedFile $photo
     * @return void
     */
    public function updateLogoFromBase64String(string $base64File)
    {
        // decode the base64 file
        $fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64File));

        // save it to temporary dir first.
        $tmpFilePath = sys_get_temp_dir() . '/' . Str::uuid()->toString();
        file_put_contents($tmpFilePath, $fileData);

        // this just to help us get file info.
        $tmpFile = new File($tmpFilePath);

        $file = new UploadedFile(
            $tmpFile->getPathname(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            0,
            true // Mark it as test, since the file isn't from real HTTP POST.
        );
        $this->updateLogo($file);

    }

    /**
     * Delete the user's profile photo.
     *
     * @return void
     */
    public function deleteLogo()
    {

        Storage::disk($this->logoDisk())->delete($this->logo);

        $this->forceFill([
            'logo' => null,
        ])->save();
    }

    /**
     * Get the URL to the user's profile photo.
     *
     * @return string
     */
    public function getLogoUrlAttribute()
    {
        return $this->logo
            ? Storage::disk($this->logoDisk())->url($this->logo)
            : $this->defaultRoundedLogoUrl();
    }

    /**
     * Get the default profile photo URL if no profile photo has been uploaded.
     *
     * @return string
     */
    protected function defaultLogoUrl()
    {
        return 'https://ui-avatars.com/api/?name=' . urlencode($this->name ?? $this->label) . '&color=7F9CF5&background=EBF4FF';
    }
    /**
     * Get the default profile photo URL if no profile photo has been uploaded.
     *
     * @return string
     */
    protected function defaultRoundedLogoUrl()
    {
        return 'https://ui-avatars.com/api/?name=' . urlencode($this->name ?? $this->label ?? $this->nom) . '&color=7F9CF5&background=EBF4FF&rounded=true&size=500';
    }

    /**
     * Get the disk that profile photos should be stored on.
     *
     * @return string
     */
    protected function logoDisk()
    {
        return isset($_ENV['VAPOR_ARTIFACT_NAME']) ? 's3' : 'public';
    }
}
