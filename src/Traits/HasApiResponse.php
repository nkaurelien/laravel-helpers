<?php


namespace Nkaurelien\Helpers\Traits;


use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;



trait HasApiResponse
{

    use HasErrorLogging;


    protected static $API_VERSION = "1.0.0";
    /**
     * @param string $message
     * @param mixed $data
     *
     * @return array
     */
    protected static function makeResponse($message, $data)
    {

        if ($data instanceof LengthAwarePaginator) {

            $arr = $data->toArray();

            $arr['success'] = true;
            $arr['message'] = $message;

            return $arr;
        }


        return [
            'success' => true,
            'message' => $message,
            'data' => $data,
        ];
    }

    /**
     * @param string $message
     * @param array $data
     *
     * @return array
     */
    protected static function makeError($message, array $data = [])
    {
        $res = [
            'success' => false,
            'message' => $message,
        ];

        if (!blank($data)) {
            $res['data'] = $data;
        }

        return $res;
    }

    public static function isInvalidHttpStatusCode($statusCode): bool
    {
        return !in_array($statusCode, array_keys(\Symfony\Component\HttpFoundation\Response::$statusTexts));

//        return $statusCode < 100 || $statusCode >= 600;
    }


    private function addExtrasInfos($array)
    {
        $array['_version'] = config('app.version',  self::$API_VERSION);
        $array['_locale'] = config('app.locale');
        return $array;
    }


    protected function sendResponse($data, $message = '', $successStatusCode = SymfonyResponse::HTTP_OK)
    {
        $res = $data;
        if (is_string($message)) {
            $res = self::makeResponse($message, $data);
        }

        try {
            $res['count'] = count($res['data']);
        } catch (Throwable $exception) {
        }

        return Response::json($this->addExtrasInfos($res), $successStatusCode);
    }

    protected function sendError($error, $statusCode = SymfonyResponse::HTTP_BAD_REQUEST, $log_error = true)
    {

        if ($log_error) {
            $this->logError($error);
        }

        $_error = [];
        if (is_string($error)) {
            $_error = self::makeError(app()->isProduction() ? __('message.errors.global') : $error);
        }

        if (is_object($error)) {

            if (is_subclass_of($error, Throwable::class)) {
                $_error = self::makeError(app()->isProduction() ? __('message.errors.global') : $error->getMessage());
                if (!app()->isProduction()) {
                    $_error['trace'] = $error->getTrace();

                }
            }

            $_error['class'] = get_real_class($error);

            if ($error instanceof ValidationException) {
                $statusCode = $error->status;
                $_error['errors'] = $error->errors();
                $_error['error'] = collect($error->errors())->flatten()->get(0);
                $_error['errorBag'] = $error->errorBag;
            } elseif ($error instanceof HttpException) {
                $statusCode = $error->getStatusCode();
            } elseif ($error instanceof ModelNotFoundException) {
                $statusCode = SymfonyResponse::HTTP_NOT_FOUND;
            } elseif ($error instanceof Throwable) {
                if (!self::isInvalidHttpStatusCode($error->getCode())) {
                    $statusCode = $error->getCode();
                }
            }
        }

        $_error['errorCode'] = $statusCode;

//        dd(self::isInvalidHttpStatusCode($statusCode), $_error);
        $statusCode = self::isInvalidHttpStatusCode($statusCode) ? SymfonyResponse::HTTP_BAD_REQUEST : $statusCode;

        return Response::json($this->addExtrasInfos($_error), $statusCode);
    }

    /**
     * Send response and Perform the given callback with exception handling.
     *
     * @param Closure $callback
     * @return mixed
     *
     */
    protected function responseWithErrorHandling(Closure $callback, $message = '', $successStatusCode = SymfonyResponse::HTTP_OK)
    {
        try {
            return $this->sendResponse($callback(), $message, $successStatusCode);
        } catch (Throwable $e) {
            $this->logError($e);
            return $this->sendError($e);
        }
    }


}
