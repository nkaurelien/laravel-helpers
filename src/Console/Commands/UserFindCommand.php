<?php

namespace Nkaurelien\Helpers\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Mail;


class UserFindCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     *  Example:
     *  php artisan user:find searchTerm
     *
     * @var string
     */
    protected $signature = 'user:find {searchTerm}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'recherche un user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {

        // $options = $this->options();

        $searchTerm = $this->argument('searchTerm');

        if (empty($searchTerm)) {
            $this->error('Give an search term to match CMD: ' . $this->signature);
        }


        $user = User::query()
            ->where('email', 'like', "%$searchTerm%")
            ->first();
        if (!$user) {
            $this->error('Aucun utilisateur touver ');
            return;
        }

        $this->info($user->toJson());

    }
}
