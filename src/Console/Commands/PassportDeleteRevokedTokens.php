<?php

namespace Nkaurelien\Helpers\Console\Commands;

use Illuminate\Console\Command;
use DB;

class PassportDeleteRevokedTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'passport:delete-revoked-tokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete Revoked Tokens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        return DB::table('oauth_access_tokens')
            ->where('revoked', true)
            ->delete();
    }
}
